# Projeto: (substitua pelo nome do projeto quando estiver definido)

## Equipe: (substitua pelo nome da equipe)

## Descrição: (descreva seu projeto quando estiver definido)

## Membros senior: (liste os membros informando nome, RA, login gitlab, universidade. siga o exemplo abaixo)

Nome Membro 01, RA_Membro_01, login_membro_01, UNICAMP

Nome Membro 02, RA_Membro_02, login_membro_02, UTFPR

Nome Membro 03, RA_Membro_03, login_membro_03, UNICAMP


## Membros junior (se houver): (liste os membros informando nome, RA, login gitlab, universidade. siga o exemplo abaixo)

Nome Membro 04, RA_Membro_04, login_membro_04, UNICAMP

Nome Membro 05, RA_Membro_05, login_membro_05, UTFPR